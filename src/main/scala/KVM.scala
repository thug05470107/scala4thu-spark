import java.io.Serializable

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

/**
  * Created by mac019 on 2017/5/8.
  */
object KeyValueApp extends App{
  val conf = new SparkConf().setAppName("KeyValue").setMaster("local[*]")
  val sc=new SparkContext(conf)

  //val kvRdd: RDD[(String, Int)] = sc.parallelize(1 to 100).map(v=>{if (v%2==0)"even"->v else "odd"->v})
  //kvRdd.foreach(println)
  //kvRdd.reduceByKey((acc,curr)=>acc+curr).foreach(println)
val lines = sc.textFile("/Users/mac019/Downloads/spark-doc.txt")
  //lines.take(40).foreach(print)
 val words = lines.flatMap(str=>str.split(" "))
     words.map(word=>word->1).reduceByKey((acc,curr)=>acc+curr).take(200).foreach(println)
  readLine()
}
