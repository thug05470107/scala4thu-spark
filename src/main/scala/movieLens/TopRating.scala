package movieLens

import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}
/**
  * Created by mac001 on 2017/5/15.
  */
object TopRatingApp extends App {
  val conf=new SparkConf().setAppName("FilterDemo").setMaster("local[*]")

  val sc=new SparkContext(conf)

  val ratings: RDD[(Int,Double)]=sc.textFile("/Users/mac001/Downloads/ml-20m/ratings.csv")
    .map(str=>str.split(","))
    .filter(_(0)!="userId")
    .map(strs=> (strs(1).toInt,strs(2).toDouble))


  val totalRatingBymovie = ratings.reduceByKey((acc,curr)=>acc+curr)
     //totalRatingBymovie.take(5).foreach(println)
  val averageRatingByMovieID = ratings.mapValues(v=>v->1)
    .reduceByKey((acc,curr)=>{(acc._1+curr._1)->(acc._2+curr._2)}).mapValues(kv=>kv._1 / kv._2.toDouble)
  //averageRatingByMovieID.take(10).foreach(println)

  val top10 =averageRatingByMovieID.sortBy (_._2,false).take(10)

//  top10.foreach(println)

  val movies =sc.textFile("/Users/mac001/Downloads/ml-20m/movies.csv")
    .map(str=>str.split(","))
    .filter(_(0)!="movieId")
    .map(strs=> (strs(0).toInt,strs(1)))

   val joined: RDD[(Int, (Double, String))]= averageRatingByMovieID.join(movies)

   joined.map(v=>v._1+","+v._2._2+","+v._2._1).saveAsTextFile("reslut")
}

